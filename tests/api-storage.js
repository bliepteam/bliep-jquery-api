module('api session storage');

test('BliepClient.APISessionStorage()', function(){
  equal(typeof $.BliepClient.APISessionStorage, 'object', 'should provide static client module for session store')
});

test('data', function(){
  var emptyData = {}
  equal(JSON.stringify(emptyData), JSON.stringify($.BliepClient.APISessionStorage.data), 'should have empty default data cache object')
});

test('delete', function(){
  $.BliepClient.APISessionStorage.data.test = true
  $.BliepClient.APISessionStorage.delete('test')
  equal(null, $.BliepClient.APISessionStorage.data.test, 'should set storage cache to null for key')
});

test('write', function(){
  $.BliepClient.APISessionStorage.write('test', true)
  equal(true, $.BliepClient.APISessionStorage.data.test, 'should set storage cache to value for key')
});

test('read', function(){
  $.BliepClient.APISessionStorage.data.test = true
  equal(true, $.BliepClient.APISessionStorage.read('test'), 'should read value from storage cache for key')
});

module('api cookie storage');

var cookies = {}
$.cookie = function(key, value, options) {
  if (value) {
    cookies[key] = value;
  } else {
    return cookies[key];
  }
}
$.removeCookie = function(key) { delete cookies[key] }

test('BliepClient.APICookieStorage()', function(){
  equal(typeof $.BliepClient.APICookieStorage, 'object', 'should provide static client module for cookie store')
});

test('defaults', function(){
  var emptyData = {}
  var apiKeys = ['accessToken', 'clientAccessToken', 'refreshToken']

  equal(JSON.stringify(emptyData), JSON.stringify($.BliepClient.APICookieStorage.data), 'should have empty default data cache object')
  equal(null, $.BliepClient.APICookieStorage.expires, 'should have null default cookie expiration (session)')
  equal(JSON.stringify(apiKeys), JSON.stringify($.BliepClient.APICookieStorage.apiKeys), 'should have 3 oauth2 token keys defined')
});

test('write', function() {
  var c = $.BliepClient.APICookieStorage.write('key', 'val');
  equal('val', cookies['key']);
  equal('val', $.BliepClient.APICookieStorage.data['key']);
  equal(null, $.BliepClient.APICookieStorage.expires);
  equal(null, c.options.expires);

  var c = $.BliepClient.APICookieStorage.write('key', 'val', 7);
  equal(7, $.BliepClient.APICookieStorage.expires);
  equal(7, c.options.expires);

  var c = $.BliepClient.APICookieStorage.write('expiring_key', 'expiring_val');
  equal('expiring_val', c.value);
  equal(7, c.options.expires);

  $.BliepClient.APICookieStorage.expires = null
  $.BliepClient.APICookieStorage.data = {}
})

test('read', function(){
  cookies['test'] = true
  equal(true, $.BliepClient.APICookieStorage.read('test'), 'should read value from cookie storage for key')
});

test('delete', function(){
  $.BliepClient.APICookieStorage.data.test = true
  cookies.test = true
  $.BliepClient.APICookieStorage.delete('test')

  equal(null, $.BliepClient.APICookieStorage.data.test, 'should set storage cache to null for key')
  equal(null, cookies.test, 'should set cookie storage to null for key')
});

