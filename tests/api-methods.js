module('api methods');

test('authenticateClient', 7, function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/', clientId: 'tester'});
  client.storage = { write: function(key, value, expires) {
    ok(true, 'should write storage for '+key+'='+value+' with '+expires+' session expiration')
  }}

  client.request = function(options) {
    equal(options.url, '127.0.0.1/oauth2/token', 'should request api endpoint')
    equal(options.type, 'POST', 'should set post method')

    data = JSON.parse(options.data)
    equal(data.client_id, 'tester', 'should use oauth2 id from client settings')
    equal(data.grant_type, 'implicit', 'should set implicit oauth2 grant type parameter')
    equal(data.response_type, 'token', 'should set oauth2 token response type parameter')
    return { "done": function (cb){
        cb({access_token: 'auth'});
    }}
  };
  client.authenticateClient();
  equal(client.ready.state(), 'resolved', 'should resolve ready state after successful authentication')
});

test('authenticate', 8, function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/', clientId: 'tester', expires: 7});
  client.storage = { write: function(key, value, expires) {
    ok(true, 'should write storage for '+key+'='+value+' expiring in '+expires+' days')
  }}
  client.request = function(options) {
    equal(options.url, '127.0.0.1/authenticate', 'should request api endpoint')
    equal(options.type, 'POST', 'should set post method')

    data = JSON.parse(options.data)
    equal(data.client_id, 'tester', 'should use oauth2 id from client settings')
    equal(data.username, 'un', 'should set useraname parameter')
    equal(data.password, 'pw', 'should set password parameter')
    return { "done": function (cb){
        cb({access_token: 'auth', refresh_token: 'fresh'});
    }}
  };
  client.authenticate('un', 'pw');
  equal(client.ready.state(), 'resolved', 'should resolve ready state after successful authentication')
});

test('registerAccount', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/registration', 'should request api endpoint')
    equal(options.type, 'POST', 'should set post method')

    data = JSON.parse(options.data)
    equal(data.name, 'nm', 'should set name parameter')
    equal(data.email, 'em', 'should set email parameter')
    equal(data.password, 'pw', 'should set password parameter')
    equal(data.sex, 'sx', 'should set sex parameter')
    equal(data.msisdn, 'mn', 'should set msisdn parameter')
    equal(data.birthdate, 'bd', 'should set birthdate parameter')
    return { "done": function (cb) {}}
  };
  client.registerAccount('nm', 'em', 'pw', 'sx', 'mn', 'bd');
});

test('orderAndRegister', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/orders', 'should request api endpoint')
    equal(options.type, 'POST', 'should set post method')

    data = JSON.parse(options.data)
    equal(data.name, 'nm', 'should set name parameter')
    equal(data.email, 'em', 'should set email parameter')
    equal(data.password, 'pw', 'should set password parameter')
    equal(data.sex, 'sx', 'should set sex parameter')
    equal(data.birthdate, 'bd', 'should set birthdate timestamp parameter')
    equal(data.numberporting, 'np', 'should set number porting parameter')
    equal(data.streetname, 'st', 'should set street name parameter')
    equal(data.housenumber, 'hn', 'should set house number parameter')
    equal(data.housenumberext, 'he', 'should set house number extension parameter')
    equal(data.cityname, 'cn', 'should set city name parameter')
    equal(data.postalcode, 'pc', 'should set postal code parameter')
    equal(data.simtype, 'sm', 'should set sim type parameter')
    equal(data.campaign_id, 'ci', 'should set campaign_id parameter')
    equal(data.redirect_url, 'ru', 'should set redirect url parameter')
    equal(data.referral_link, 'rc', 'should set referral code parameter')
    return { "done": function (cb) {}}
  };
  client.orderAndRegister('nm', 'em', 'pw', 'sx', 'bd', 'np', 'st', 'hn', 'he', 'cn', 'pc', 'sm', 'ci', 'ru', 'rc');
});

test('confirmAccount', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/registration', 'should request api endpoint')
    equal(options.type, 'PUT', 'should set put method')

    data = JSON.parse(options.data)
    equal(data.confirmcode, 'cd', 'should set code parameter')
    equal(data.msisdn, 'mn', 'should set msisdn parameter')
    return { "done": function (cb) {}}
  };
  client.confirmAccount('cd', 'mn');
});

test('updateSIMCardState', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile/state', 'should request api endpoint')
    equal(options.type, 'PUT', 'should set put method')

    data = JSON.parse(options.data)
    equal(data.on, 'bs', 'should set state parameter')
    equal(data.plus, 'bp', 'should set plus parameter')
    equal(data.voice, 'bv', 'should set voice parameter')
    return { "done": function (cb) {}}
  };
  client.updateSIMCardState('bs', 'bp', 'bv');
});

test('listPortingProviders', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/porting/providers', 'should request api endpoint')
    equal(options.type, 'GET', 'should set get method')
    return { "done": function (cb) {}}
  };
  client.listPortingProviders();
});

test('createPortIn', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/porting', 'should request api endpoint')
    equal(options.type, 'POST', 'should set post method')

    data = JSON.parse(options.data)
    equal(data.provider, 'pv', 'should set provider parameter')
    equal(data.iccid, 'ic', 'should set iccid parameter')
    equal(data.msisdn, 'mn', 'should set msisdn parameter')
    equal(data.contract_type, 'ct', 'should set contract type parameter')
    equal(data.orderid, 'oi', 'should set order id parameter')
    equal(data.ordersecret, 'os', 'should set order secret parameter')
    return { "done": function (cb) {}}
  };
  client.createPortIn('pv', 'ic', 'mn', 'ct', 'oi', 'os');
});

test('getProviderSimFormat', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/porting/simformat', 'should request api endpoint')
    equal(options.type, 'GET', 'should set get method')
    equal(options.data.provider_code, 'pc', 'should set provider code parameter')

    return { "done": function (cb) {}}
  };
  client.getProviderSimFormat('pc');
});

test('checkProviderSimFormat', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/porting/simformat/check', 'should request api endpoint')
    equal(options.type, 'GET', 'should set get method')
    equal(options.data.provider_code, 'pc', 'should set provider code parameter')
    equal(options.data.iccid, 'ic', 'should set iccid parameter')

    return { "done": function (cb) {}}
  };
  client.checkProviderSimFormat('pc', 'ic');
});

test('listCreditHistory', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile/credit/history', 'should request api endpoint')
    equal(options.type, 'GET', 'should set get method')
    equal(options.data.limit, 10, 'should set limit parameter')
    equal(options.data.offset, 20, 'should set offset parameter')

    return { "done": function (cb) {}}
  };
  client.listCreditHistory(20, 10);
});

test('shareCredit', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile/credit/share', 'should request api endpoint')
    equal(options.type, 'POST', 'should set post method')

    data = JSON.parse(options.data)
    equal(data.amount, 3, 'should set amount parameter')
    equal(data.msisdn, 'mn', 'should set msisdn parameter')
    equal(data.addressbook, 'ad', 'should set address book parameter')
    equal(data.personal_message, 'msg', 'should set personal message parameter')
    return { "done": function (cb) {}}
  };
  client.shareCredit(3, 'mn', 'ad', 'msg');
});

test('listSharingAddressBook', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile/credit/share/addressbook', 'should request api endpoint')
    equal(options.type, 'GET', 'should set get method')

    return { "done": function (cb) {}}
  };
  client.listSharingAddressBook();
});

test('getRecurringPaymentSettings', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile/settings/recurring_payment', 'should request api endpoint')
    equal(options.type, 'GET', 'should set get method')

    return { "done": function (cb) {}}
  };
  client.getRecurringPaymentSettings();
});

test('setRecurringPaymentSettings', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile/settings/recurring_payment', 'should request api endpoint')
    equal(options.type, 'PUT', 'should set put method')

    data = JSON.parse(options.data)
    equal(data.monthly.enabled, true, 'should set monthly enabled parameter')
    equal(data.monthly.amount, 5, 'should set monthly amount parameter')
    equal(data.lowcredit.enabled, false, 'should set lowcredit enabled parameter')
    equal(data.lowcredit.amount, 10, 'should set lowcredit amount parameter')
    return { "done": function (cb) {}}
  };
  client.setRecurringPaymentSettings(true, 5, false, 10);
});

test('disableRecurringContract', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile/settings/recurring_payment', 'should request api endpoint')
    equal(options.type, 'DELETE', 'should set delete method')

    return { "done": function (cb) {}}
  };
  client.disableRecurringContract();
});

test('topupCredit', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile/credit/web', 'should request api endpoint')
    equal(options.type, 'POST', 'should set post method')

    data = JSON.parse(options.data)
    equal(data.amount, 3, 'should set amount parameter')
    equal(data.redirect_url, 'localhost', 'should set redirect url parameter')
    equal(data.recurring, false, 'should set recurring payment parameter')
    return { "done": function (cb) {}}
  };
  client.topupCredit(3, 'localhost', false);
});

test('topupVoucherCredit', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile/credit/voucher', 'should request api endpoint')
    equal(options.type, 'POST', 'should set post method')

    data = JSON.parse(options.data)
    equal(data.voucher_code, 'vc', 'should set voucher code parameter')
    return { "done": function (cb) {}}
  };
  client.topupVoucherCredit('vc');
});

test('listCallHistory', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile/call/history', 'should request api endpoint')
    equal(options.type, 'GET', 'should set get method')
    equal(options.data.limit, 10, 'should set limit parameter')
    equal(options.data.offset, 20, 'should set offset parameter')

    return { "done": function (cb) {}}
  };
  client.listCallHistory(20, 10);
});

test('viewSettings', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile/settings', 'should request api endpoint')
    equal(options.type, 'GET', 'should set get method')

    return { "done": function (cb) {}}
  };
  client.viewSettings();
});

test('updateCredentials', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile', 'should request api endpoint')
    equal(options.type, 'PUT', 'should set put method')

    data = JSON.parse(options.data)
    equal(data.email, 'em', 'should set email parameter')
    equal(data.password, 'pw', 'should set password parameter')
    equal(data.new_password, 'np', 'should set new password parameter')
    return { "done": function (cb) {}}
  };
  client.updateCredentials('em', 'pw', 'np');
});

test('updateSettings', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile/settings', 'should request api endpoint')
    equal(options.type, 'PUT', 'should set put method')

    data = JSON.parse(options.data)
    equal(data.call_from_balance, 'cb', 'should set call from balance parameter')
    equal(data.voicemail_active, 'vm', 'should set voicemail active parameter')
    return { "done": function (cb) {}}
  };
  client.updateSettings('cb', 'vm');
});

test('viewRoamingSettings', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile/settings/roaming', 'should request api endpoint')
    equal(options.type, 'GET', 'should set get method')

    return { "done": function (cb) {}}
  };
  client.viewRoamingSettings();
});

test('updateRoamingSettings', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile/settings/roaming', 'should request api endpoint')
    equal(options.type, 'PUT', 'should set put method')

    data = JSON.parse(options.data)
    equal(data.test, true, 'should send arbitrary settings parameters')
    return { "done": function (cb) {}}
  };
  client.updateRoamingSettings({'test': true});
});

test('viewProfile', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile', 'should request api endpoint')
    equal(options.type, 'GET', 'should set get method')

    return { "done": function (cb) {}}
  };
  client.viewProfile();
});

test('forgotLogin', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile/forgot_login', 'should request api endpoint')
    equal(options.type, 'POST', 'should set post method')

    data = JSON.parse(options.data)
    equal(data.email, 'em', 'should set email parameter')
    equal(data.msisdn, 'mn', 'should set msisdn parameter')
    return { "done": function (cb) {}}
  };
  client.forgotLogin('em', 'mn');
});

test('viewReferral', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile/referral', 'should request api endpoint')
    equal(options.type, 'GET', 'should set get method')

    return { "done": function (cb) {}}
  };
  client.viewReferral();
});

test('viewSignature', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile/signature', 'should request api endpoint')
    equal(options.type, 'GET', 'should set get method')

    return { "done": function (cb) {}}
  };
  client.viewSignature();
});

test('updateSignature', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/profile/signature', 'should request api endpoint')
    equal(options.type, 'PUT', 'should set put method')

    data = JSON.parse(options.data)
    equal(data.signature, 'sg', 'should set signature parameter')
    equal(data.use, 'us', 'should set use parameter')
    return { "done": function (cb) {}}
  };
  client.updateSignature('sg', 'us');
});

test('listFAQ highlighted', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/help/faq', 'should request api endpoint')
    equal(options.type, 'GET', 'should set get method')

    return { "done": function (cb) {}}
  };
  client.listFAQ();
});

test('listFAQ categorized', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/help/faq', 'should request api endpoint')
    equal(options.type, 'GET', 'should set get method')
    equal(options.data.category, 'cid', 'should set category id parameter')

    return { "done": function (cb) {}}
  };
  client.listFAQ('cid');
});

test('searchFAQ', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/help/search', 'should request api endpoint')
    equal(options.type, 'GET', 'should set get method')
    equal(options.data.text, 'txt', 'should set search text parameter')

    return { "done": function (cb) {}}
  };
  client.searchFAQ('txt');
});

test('askQuestion', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  client.request = function(options) {
    equal(options.url, '127.0.0.1/help/faq', 'should request api endpoint')
    equal(options.type, 'POST', 'should set post method')

    data = JSON.parse(options.data)
    equal(data.name, 'nm', 'should set name parameter')
    equal(data.email, 'em', 'should set email parameter')
    equal(data.message, 'mg', 'should set message parameter')
    return { "done": function (cb) {}}
  };
  client.askQuestion('nm', 'em', 'mg');
});

test('checkEmail', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  var email = 'test@example.com';
  client.request = function(options) {
    equal(options.url, '127.0.0.1/check_email', 'should request api endpoint')
    equal(options.type, 'GET', 'should set get method')
    equal(options.data.email, email, 'should set email lookup parameter')

    return { "done": function (cb) {}}
  };
  client.checkEmail(email);
});

test('geolocateAddress', function(){
  var client = new $.BliepClient({baseUrl: '127.0.0.1/'});
  var postalcode = '1012MC';
  var housenumber = 99;
  client.request = function(options) {
    equal(options.url, '127.0.0.1/check_postcode', 'should request api endpoint')
    equal(options.type, 'GET', 'should set get method')
    equal(options.data.postalcode, postalcode, 'should set email lookup parameter')
    equal(options.data.housenumber, housenumber, 'should set house number lookup parameter')

    return { "done": function (cb) {}}
  };
  client.geolocateAddress(postalcode, housenumber);
});